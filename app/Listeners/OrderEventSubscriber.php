<?php

namespace App\Listeners;

use App\Events\OrderStatusUpdatedEvent;
use App\Mail\OrderStatusUpdatedMail;
use Illuminate\Events\Dispatcher;

class OrderEventSubscriber
{
    /**
     * Order update
     * @param $event
     * @return mixed
     */
    public function onOrderUpdate($event)
    {
        info($event->order->getOriginal('status'));
        return \Mail::to($event->order->user)
            ->send(new OrderStatusUpdatedMail($event->order));
    }

    public function subscribe(Dispatcher $events)
    {
        $events->listen(
            [OrderStatusUpdatedEvent::class],
            'App\Listeners\OrderEventSubscriber@onOrderUpdate'
            );
    }
}