<?php

namespace App\Http\Controllers;


use App\Http\Requests\OrderUpdateRequest;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    /**
     * List of orders
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('view',Order::class);
        $orders = Order::paginate();

        return OrderResource::collection($orders);
    }


    /**
     * Update order
     * @param Order $order
     * @param OrderUpdateRequest $request
     * @return OrderResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Order $order,OrderUpdateRequest $request)
    {
        $this->authorize('update',$order);
        $order->update($request->only(['status']));

        return OrderResource::make($order);
    }
}