<?php

namespace App\Policies;

use App\Models\Order;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderPolicy
{
    use HandlesAuthorization;

    /**
     * Display order policy
     * @param User $user
     * @return bool
     */
    public function view(User $user)
    {
        return true; // TODO: implement checking user permissions
    }

    /**
     * Update order data policy
     * @param User $user
     * @param Order $order
     * @return bool
     */
    public function update(User $user, Order $order)
    {
        return $user->id === $order->user_id;
    }
}
