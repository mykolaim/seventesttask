<?php

namespace App\Models;

use App\Events\OrderStatusUpdatedEvent;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

/**
 * Class Order
 * @property-read int $id
 * @property-read int $user_id
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @package App\Models
 */
class Order extends Model
{
     use Notifiable;

     protected $dispatchesEvents = [
       'updated' => OrderStatusUpdatedEvent::class
     ];

    const STATUS_PENDING = 'pending';
    const STATUS_DONE = 'done';
    const STATUS_DECLINED = 'declined';

    public $fillable = ['status'];

    /**
     * Get array of order statuses
     * @return array
     */
     public static function getStatuses() :array
     {
         return [
             self::STATUS_PENDING,
             self::STATUS_DONE,
             self::STATUS_DECLINED
         ];
     }

    /**
     * User relation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
     public function user()
     {
         return $this->belongsTo(User::class,'user_id');
     }

    /**
     * Products relation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
     public function products()
     {
         return $this->belongsToMany(Product::class,'order_product','order_id','product_id');
     }
}
