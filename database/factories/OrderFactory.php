<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Order::class, function (Faker $faker) {
    $users = \App\Models\User::all();

    return [
        'user_id' => $users->random(1)->first()->id
    ];
});
