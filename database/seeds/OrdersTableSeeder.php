<?php

use Illuminate\Database\Seeder;
use App\Models\Order;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $prodcuts = \App\Models\Product::all();
        factory(Order::class, 5)->create(
        )->each(function (Order $order) use($prodcuts){
           $order->products()->saveMany($prodcuts->random(random_int(1,5))->all());
        });
    }
}
